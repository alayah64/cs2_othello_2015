#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int score(Board b, Move *m, Side s, Side oppSide);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

private:
    Side s;
    Side oppSide;
    Board b;
};

#endif
