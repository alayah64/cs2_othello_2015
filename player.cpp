#include "player.h" // This is a small change!
#include <map>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    b = Board();
    s = side;
    if (s == WHITE)
        oppSide = BLACK;
    else
        oppSide = WHITE;
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    b.doMove(opponentsMove, oppSide);
    if (!b.hasMoves(s))
        return NULL;
    /*
    
    Move *bestMove;
    Move *tempMove;
    int bestMoveScore = -1;
    int tempMoveScore;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            tempMove = new Move(i, j);
            if (b.checkMove(tempMove, s))
            {
                tempMoveScore = score(tempMove);
                if (tempMoveScore > bestMoveScore)
                {
                    bestMove = tempMove;
                    bestMoveScore = tempMoveScore;
                }
            }
        }
    }
    b.doMove(bestMove, s);
    return bestMove; */

    /* minimax attempt */
    Move *tempMove;
    std::map<Move*, Move*> responses;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            tempMove = new Move(i, j);
            if (b.checkMove(tempMove, s))
                responses[tempMove] = new Move(0, 0);
        }
    }

    int tempOpponentMoveScore;
    int bestOpponentMoveScore;
    Move *tempOpponentMove;
    Move *bestOpponentMove;
    Board *tempBoard;
    for (std::map<Move*, Move*>::iterator it = responses.begin(); it != responses.end(); ++it)
    {
        bestOpponentMoveScore = -1;
        tempBoard = b.copy();
        tempBoard->doMove((*it).first, s);
        if (tempBoard->hasMoves(oppSide))
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    tempOpponentMove = new Move(i, j);
                    if (tempBoard->checkMove(tempOpponentMove, oppSide))
                    {
                        tempOpponentMoveScore = score(*tempBoard, tempOpponentMove, oppSide, s);
                        if (tempOpponentMoveScore > bestOpponentMoveScore)
                        {
                            bestOpponentMoveScore = tempOpponentMoveScore;
                            bestOpponentMove = tempOpponentMove;
                        }
                    }
                }
            }
            responses[(*it).first] = bestOpponentMove;
        }
        else
            responses[(*it).first] = NULL;
    }

    std::map<Move*, int> updatedMoveScores;
    int tempMoveScore;
    int bestMoveScore = -1;
    for (std::map<Move*, Move*>::iterator it = responses.begin(); it != responses.end(); ++it)
    {
        tempBoard = b.copy();
        tempBoard->doMove((*it).first, s);
        if ((*it).second != NULL)
          tempBoard->doMove((*it).second, oppSide);
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                tempMove = new Move(i, j);
                if (tempBoard->checkMove(tempMove, s))
                {
                    tempMoveScore = score(*tempBoard, tempMove, s, oppSide);
                    if (tempMoveScore > bestMoveScore)
                        bestMoveScore = tempMoveScore;
                }
            }
        }
        updatedMoveScores[(*it).first] = bestMoveScore + score(b, (*it).first, s, oppSide);
    }

    int i = -1;
    Move *retVal;
    for (std::map<Move*, int>::iterator it = updatedMoveScores.begin(); it != updatedMoveScores.end(); ++it)
    {
        if ((*it).second > i)
        {
            retVal = (*it).first;
            i = (*it).second;
        }
    }
    b.doMove(retVal, s);
    return retVal;
}

int Player::score(Board b, Move *m, Side s, Side oppSide)
{
    /*
    int i = m->x;
    int j = m->y;
    if (((i == 0) || (i == 7)) && ((j == 0) || (j == 7)))
        return 100000;
    int sco = 0;
    Board *tempBoard = b.copy();
    tempBoard -> doMove(m, s);
    sco = sco + tempBoard->getMobility(s);
    if ((tempBoard->checkMove(new Move(0, 0), oppSide)) && !(b.checkMove(new Move(0, 0), oppSide)))
        sco = sco / 10;
    if ((tempBoard->checkMove(new Move(0, 7), oppSide)) && !(b.checkMove(new Move(0, 7), oppSide)))
        sco = sco / 10;
    if ((tempBoard->checkMove(new Move(7, 0), oppSide)) && !(b.checkMove(new Move(7, 0), oppSide)))
        sco = sco / 10;
    if ((tempBoard->checkMove(new Move(7, 7), oppSide)) && !(b.checkMove(new Move(7, 7), oppSide)))
        sco = sco / 10;

    delete tempBoard;
    return sco;*/
    Board *tempBoard = b.copy();
    if (tempBoard->checkMove(m, s))
        tempBoard -> doMove(m, s);
    else
        return -1;
    int sco = 100 - tempBoard->getMobility(oppSide);
    int i = m->x;
    int j = m->y;
    if (((i == 0) || (i == 7)) && ((j == 0) || (j == 7)))
        return 1000;
    if ((tempBoard->checkMove(new Move(0, 0), oppSide)) && !(b.checkMove(new Move(0, 0), oppSide)))
        sco = sco - 5;
    if ((tempBoard->checkMove(new Move(0, 7), oppSide)) && !(b.checkMove(new Move(0, 7), oppSide)))
        sco = sco - 5;
    if ((tempBoard->checkMove(new Move(7, 0), oppSide)) && !(b.checkMove(new Move(7, 0), oppSide)))
        sco = sco - 5;
    if ((tempBoard->checkMove(new Move(7, 7), oppSide)) && !(b.checkMove(new Move(7, 7), oppSide)))
        sco = sco - 5;
    sco = sco + tempBoard->count(s) / 4;
    return sco;
}